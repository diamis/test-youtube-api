# Тестовое задание 

## Задача:
Необходимо сделать поиск видео по названию через API YouTube, при открытии видео показывает его через frame и сохраняет в БД, далее сохраненное видео участвует так же в поиске.

## Запуск приложения:

> `git clone git@gitlab.com:diamis/test-youtube-api.git`

> `cd test-youtube-api`

### 1 Настройка:

> `yarn install`

> `cp .env.exemple .env`

### 2 Контейнер:

> `docker-compose up --build`

### 3 Мигарции:

> `docker-compose exec serve bash`

> `node migration.js up`

### 4 Старт:

> откройте браузер по адресу указанному в консоле `http://localhost:3030`



