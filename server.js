const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mysql = require("mysql");
const _ = require("lodash");

require("dotenv").config();

const app = express();
const PORT = process.env.REACT_APP_PORT_SERVER;

let connection;
let timerCount = 0;
let timerConnect = null;
const connect = () => {
    connection = mysql.createConnection({
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME
    });
    connection.connect(err => {
        clearTimeout(timerConnect);
        if (err) {
            timerCount++;
            if (timerCount < 20) {
                timerConnect = setTimeout(() => connect(), 10000);
            }
            return console.error("Error DB: " + err.message);
        }
        console.log("Подключение к серверу MySQL успешно установлено");
    });
};

connect();

app.use(cors());
app.use(bodyParser.json());

app.get("/search", (req, res) => {
    const q = _.get(req, "query.q");
    const sql = "SELECT * FROM videos WHERE title LIKE '%" + q + "%' LIMIT 5";
    if (!q) return res.send({ error: "Не задан параметр q" });
    connection.query(sql, (err, result) => {
        if (err) return res.send({ error: "Возникла ошибка при записи в базу" });
        res.send({ result: result });
    });
});

app.post("/search/add", (req, res) => {
    const items = _.get(req, "body") || [];

    const sql = "INSERT INTO `videos` (youtubeId, path, title, description) VALUES ?";
    const values = items.reduce((value, item) => {
        value.push([
            _.get(item, "youtubeId") || null,
            _.get(item, "path") || null,
            _.get(item, "title") || null,
            _.get(item, "description") || null
        ]);
        return value;
    }, []);

    connection.query(sql, [values], (err, result) => {
        if (err) return res.send({ error: "Возникла ошибка при записи в базу" });
        res.send({ result: result.affectedRows });
    });
});

app.listen(PORT, () => console.log(`http://127.0.0.1:${PORT}`));
