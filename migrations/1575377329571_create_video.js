let params = [];
params.push("id int");
params.push("youtubeId varchar(255)");
params.push("path varchar(255)");
params.push("title varchar(255)");
params.push("description varchar(255)");

module.exports = {
    up: `CREATE TABLE videos (${params.join(",")})`,
    down: "DROP TABLE videos"
};
