import React from "react";

import { Layout } from "components/Layout";

function Root() {
    return (
        <Layout>
            <h3>Задача:</h3>
            <p>
                Необходимо сделать поиск видео по названию через API YouTube, при открытии видео показывает его через frame и сохраняет
                в БД, далее сохраненное видео участвует так же в поиске.
            </p>
        </Layout>
    );
}

export default Root;
