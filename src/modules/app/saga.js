import { put, all, call, takeLatest } from "redux-saga/effects";
import { get } from "lodash";

import { APP } from "./constants";
import { appSearchFailure, appSearchSuccess } from "./actions";
import { apiSearchGoogle, apiSearchAdd, apiSearch } from "./api";

export default function*() {
    yield all([yield takeLatest(APP.SEARCH.REQUEST, searchResults)]);
}

function* searchResults(action) {
    try {
        const { payload } = action;
        if (!payload) return;

        let search = {};
        let results = [];

        console.group("request search");

        /** Request REST API MyApp **/
        const appResult = yield call(apiSearch, payload);
        const appData = get(appResult, "data.result");
        if (appData && appData.length) {
            console.log("appData:", appData);

            for (let item of appData) {
                search[item.youtubeId] = true;
                results.push({
                    id: { videoId: item.youtubeId },
                    snippet: {
                        title: item.title,
                        description: item.description
                    }
                });
            }
        }

        /** Request REST API YouTube **/
        const apiResult = yield call(apiSearchGoogle, payload);
        const apiData = get(apiResult, "result.items") || [];
        if (apiData && apiData.length) {
            console.log("apiData:", apiData);

            const insert = [];
            for (let item of apiData) {
                const videoId = get(item, "id.videoId");

                if (typeof search[videoId] === "undefined") {
                    results.push(item);
                    insert.push(item);
                }
            }

            if (insert.length) yield searchInsert({ payload: apiData });
        }

        console.log("results:", results);
        console.groupEnd();

        yield put(appSearchSuccess(results));
    } catch ({ message }) {
        yield put(appSearchFailure(message));
    }
}

function* searchInsert(action) {
    try {
        const { payload } = action;
        if (!payload) return;

        /** Insert REST API MyApp **/
        const data = payload.reduce((newArr, item) => {
            newArr.push({
                youtubeId: get(item, "id.videoId"),
                title: get(item, "snippet.title"),
                description: get(item, "snippet.description")
            });
            return newArr;
        }, []);

        yield call(apiSearchAdd, data);
    } catch ({ message }) {
        yield put(appSearchFailure(message));
    }
}
