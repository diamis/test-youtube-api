import { youtube, get, post } from "services/request";

export const apiSearch = params => get("/search", { params });
export const apiSearchAdd = data => post("/search/add", { data });
export const apiSearchGoogle = params => youtube(params);
