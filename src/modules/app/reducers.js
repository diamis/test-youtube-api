import { APP } from "./constants";

const data = {
    loading: false,
    result: null,
    error: null
};
const initaleState = {
    search: data
};

export default function rootReducer(state = initaleState, action) {
    const { type, payload } = action;
    if (type === APP.SEARCH.REQUEST) return { ...state, search: { ...state.search, loading: true } };
    if (type === APP.SEARCH.SUCCESS) return { ...state, search: { ...data, result: payload } };
    if (type === APP.SEARCH.FAILURE) return { ...state, search: { ...state.search, loading: false, error: payload } };

    return state;
}
