import { createAction } from "redux-actions";
import { APP } from "./constants";

export const appSearchRequest = createAction(APP.SEARCH.REQUEST);
export const appSearchSuccess = createAction(APP.SEARCH.SUCCESS);
export const appSearchFailure = createAction(APP.SEARCH.FAILURE);
