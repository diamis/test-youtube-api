import actionTypeCreate, { ASYNC } from "redux-action-types-creator";

const actionType = actionTypeCreate("APP");
const APP = actionType({
    SEARCH: ASYNC
});

export { APP };
