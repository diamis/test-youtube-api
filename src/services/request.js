import axios from "axios";

const ApiGoogleInit = () => {
    const gapi = window.gapi;
    if (gapi) return gapi;

    const script = document.createElement("script");
    script.src = "//apis.google.com/js/client.js";
    script.onload = () => {
        const gapi = window.gapi;
        gapi.load("client:auth2", () => {
            gapi.client.setApiKey("AIzaSyCl0XZUL8sm228SeLSlEU78Y-Nn6hEmaGw");
            gapi.client.load("youtube", "v3");
        });
    };

    document.body.appendChild(script);
};

ApiGoogleInit();

const ENV = process.env;
const request = (method, url, { params, data }) => {
    const host = `//${ENV.REACT_APP_HOST_SERVER}:${ENV.REACT_APP_PORT_SERVER}`;
    const requestData = {
        url: host + url,
        method,
        params,
        data
    };

    return axios(requestData);
};

export const youtube = params => {
    return window.gapi.client.youtube.search.list({ ...params });
};

export const post = (url, data) => {
    return request("POST", url, data);
};

export const get = (url, data) => {
    return request("GET", url, data);
};
