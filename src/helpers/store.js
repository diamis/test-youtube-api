import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";

import rootSaga from "./createSaga";
import rootReducer from "./createReducer";

let composeEnhancers = compose;
const middleware = [];
if (process.env.NODE_ENV !== "production") {
    /* Redux Logger */
    if (process.env.REACT_APP_REDUX_LOGGER === "true") {
        const { createLogger } = require("redux-logger");
        middleware.push(createLogger({ collapsed: true }));
    }

    /* Redux DevTools */
    if (process.env.REACT_APP_DEVTOOLS === "true" && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__)
        composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
}

/* SAGA MIDDLEWARE */
const sagaMiddleware = createSagaMiddleware();
middleware.push(sagaMiddleware);
const store = createStore(rootReducer, composeEnhancers(applyMiddleware(...middleware)));

sagaMiddleware.run(rootSaga);

export default store;
