import { all } from "redux-saga/effects";

import app from "modules/app/saga";

export default function* rootSaga() {
    yield all([app()]);
}
