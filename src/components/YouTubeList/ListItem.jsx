import React from "react";
import { get } from "lodash";

export const ListItem = props => {
    const videoId = get(props, "id.videoId");
    const title = get(props, "snippet.title");
    const channelTitle = get(props, "snippet.channelTitle");
    return (
        <div className="YouTube__item">
            <div className="YouTube__video">
                <iframe
                    title={title}
                    type="text/html"
                    width="300"
                    height="105"
                    src={`http://www.youtube.com/embed/${videoId}`}
                    frameBorder="0"
                />
            </div>
            <div className="YouTube__content">
                <div className="YouTube__title">{title}</div>
                <div className="YouTube__chanel">{channelTitle}</div>
            </div>
        </div>
    );
};
