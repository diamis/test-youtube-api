import React from "react";
import { get } from "lodash";
import { Icon } from "antd";

import { ListItem } from "./ListItem";
import "./YouTubeList.scss";

class YouTubeList extends React.Component {
    render() {
        const { loading, items } = this.props;
        if (loading) return <Icon type="loading" style={{ fontSize: "32px", color: "#08c" }} />;
        return (
            <div className="YouTube_list">
                {items && items.map((item, i) => <ListItem key={get(item, "id.videoId") + i || i} {...item} />)}{" "}
            </div>
        );
    }
}

export default YouTubeList;
