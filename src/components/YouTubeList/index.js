import { connect } from "react-redux";
import BaseYouTubeList from "./YouTubeList";

const mapStateToProps = state => ({
    items: state.app.search.result,
    loading: state.app.search.loading
});

const YouTubeList = connect(mapStateToProps)(BaseYouTubeList);
export { YouTubeList };
