import React from "react";

import Header from "./Header";
import { YouTubeList } from "components/YouTubeList";

import "antd/dist/antd.css";
import "./Layout.scss";

class Layout extends React.PureComponent {
    render() {
        const { loading, children } = this.props;
        return (
            <>
                <Header requestSearch={this.props.requestSearch} />
                <main>{loading ? "loading..." : <YouTubeList />}</main>
                <div className="footer">{children}</div>
            </>
        );
    }
}

export default Layout;
