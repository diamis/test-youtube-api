import { connect } from "react-redux";

import { appSearchRequest } from "modules/app/actions";
import BaseLayout from "./Layout";

const mapStateToProps = state => ({});
const mapDispatchToProps = dispatch => ({
    requestSearch: data => dispatch(appSearchRequest(data))
});
const Layout = connect(mapStateToProps, mapDispatchToProps)(BaseLayout);
export { Layout };
