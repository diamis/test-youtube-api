import React from "react";
import { Icon } from "antd";

import { InputSearch } from "components/InputSearch";
import "./Header.scss";

class Header extends React.PureComponent {
    render() {
        return (
            <header className="Header">
                <div className="HeaderLogo">
                    <Icon type="youtube" style={{ fontSize: "28px", color: "#08c" }} />
                    <span>Test API</span>
                </div>
                <div className="HeaderSearch">
                    <InputSearch onSearch={this.props.requestSearch} />
                </div>
            </header>
        );
    }
}

export default Header;
