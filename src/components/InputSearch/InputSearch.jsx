import React from "react";
import { Input } from "antd";

import "./InputSearch.scss";
const { Search } = Input;

class InputSearch extends React.PureComponent {
    handleOnSearch = val => {
        const { onSearch } = this.props;
        onSearch({ order: "viewCount", part: "snippet", q: val, type: "video", videoDefinition: "high" });
    };
    render() {
        return (
            <div className="InputSearch">
                <Search onSearch={this.handleOnSearch} />
            </div>
        );
    }
}

InputSearch.defaultProps = {
    onSearch: () => {}
};

export default InputSearch;
