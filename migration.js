const mysql = require("mysql");
const migration = require("mysql-migrations");

require("dotenv").config();

const connection = mysql.createPool({
    connectionLimit: 10,
    host: process.env.DB_HOST,
    user: process.env.DB_USER,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD
});

migration.init(connection, __dirname + "/migrations");
